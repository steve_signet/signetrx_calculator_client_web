/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.controller;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author sburns
 */  
public class Service implements Runnable {
    private final String command;
    private final VirtualMachine virtualMachine;
    private static final Logger logger = Logger.getLogger("calculatorcontroller.log");
    
    private final int timeout;  // 10 second time out. Time in milliseconds          
    private static final String serviceName = "SignetRxCalculatorClient";
    
    /**
     * Manages Virtual Machines on Remote Servers
     * 
     * @param command  start, stop, or query command
     * @param virtualMachine
     */
    public Service(final String command, final VirtualMachine virtualMachine) {
        this.command = command;
        this.virtualMachine = virtualMachine;
        /*
        CalculatorControllerPropertiesManager calculatorControllerPropertiesManager = 
                new CalculatorControllerPropertiesManager();
        Properties timeoutProperties = calculatorControllerPropertiesManager.loadParams();
        timeout = Integer.parseInt((String) timeoutProperties.get("timeout.milliseconds"));
        logger.log(Level.WARNING, "Timeout: {0}", timeout);
        */
        timeout = 1000;
    }
    
    @Override
    public void run() {
        State state = State.INDETERMINATE;
        
        if (command.equals("start")) {
            state = startService(virtualMachine.getName());
        } else if (command.equals("stop")) {
            state = stopService(virtualMachine.getName());
        } else if (command.equals("kill")) {
            try {
                state = killService(virtualMachine.getName());
            } catch (IOException ex) {
                Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (command.equals("query")) {
            state = queryService(virtualMachine.getName());
        }
        
        if (state == State.RUNNING) {                      
            virtualMachine.setStatus("Running");
        } else if (state == State.STOPPED) {
            virtualMachine.setStatus("Stopped");
        } else if (state == State.ACCESS_DENIED) {
            virtualMachine.setStatus("Access Denied");
        } else {
            virtualMachine.setStatus("Indeterminate");
        }          
    }
       
    /**
     * Starts service on virtual machine
     * 
     * @param virtualMachineName  remote machine name
     * @return 
     */
    private State startService(String virtualMachineName) {                
        final Runtime runtime = Runtime.getRuntime();
                
        try {
            final String startCommand = "sc \\\\" + virtualMachineName + " start " + serviceName;
            logger.log(Level.WARNING, "Running command: {0}", startCommand);
            
            runtime.exec(startCommand);
            
            State state = State.INDETERMINATE;
            
            final long startTime = System.currentTimeMillis();
            
            while ((System.currentTimeMillis() - startTime) < timeout) {
                state = queryService(virtualMachineName);
                
                if (state == State.RUNNING) {
                    logger.log(Level.WARNING, "Start service: Virtual machine {0} running.", virtualMachineName);
                    break;
                } else if (state == State.ACCESS_DENIED) {
                    logger.log(Level.WARNING, "Start service: Virtual machine {0} access denied.", virtualMachineName);
                    break;
                }
            }
            
            return state;
        } catch (IOException ex) {
            logger.log(Level.WARNING, "message: {0}", ex.getLocalizedMessage());
            logger.log(Level.WARNING, "Cannot start virtual machine: {0}: {1}", 
                    new Object[]{virtualMachineName, ex.getLocalizedMessage()});
            return State.INDETERMINATE;
        }       
     }
        
    /**
     * Stops service on virtual machine
     * 
     * @param virtualMachineName
     * @return 
     */
    private State stopService(String virtualMachineName) {        
        final Runtime runtime = Runtime.getRuntime();
                
        try {
            final String stopCommand = "sc \\\\" + virtualMachineName + " stop " + serviceName;
            logger.log(Level.WARNING, "Running command: {0}", stopCommand);
            
            runtime.exec(stopCommand);
            
            State state = State.INDETERMINATE;
            
            final long startTime = System.currentTimeMillis();
            
            while ((System.currentTimeMillis() - startTime) < timeout) {
                state = queryService(virtualMachineName);
                
                if (state == State.STOPPED) {
                    logger.log(Level.WARNING, "Stop service: Virtual machine {0} stopped.", virtualMachineName);
                    break;
                } else if (state == State.ACCESS_DENIED) {
                    logger.log(Level.WARNING, "Stop service: Virtual machine {0} access denied.", virtualMachineName);
                    break;
                }
            }
            
            return state;
        } catch (IOException ex) {
            logger.log(Level.WARNING, "message: {0}", ex.getLocalizedMessage());
            logger.log(Level.WARNING, "Cannot stop virtual machine: {0}: {1}", new Object[]{virtualMachineName, ex.getLocalizedMessage()});
            return State.INDETERMINATE;
        } 
    }
    
    /**
     * Stop service with kill file
     * 
     * @param virtualMachineName
     * @return 
     */
    private State killService(String virtualMachineName) throws IOException {                    
        final VirtualMachinePropertiesManager propertiesManager = new VirtualMachinePropertiesManager();
        final Properties properties = propertiesManager.loadParams();
        
        final String killFileName = properties.getProperty("killFileName");
        
        final String sourceDirectoryPath = properties.getProperty("killFileSourceDirectoryPath");
        final String sourceFileName = sourceDirectoryPath + File.separator + killFileName;
        final File sourceFile = new File(sourceFileName);

        final String destinationDirectoryPath = properties.getProperty("killFileDestinationDirectoryPath");
        final String desintationDirectoryName = destinationDirectoryPath + File.separator + virtualMachineName;
        final File destinationDirectory = new File(desintationDirectoryName);

        logger.warning("Running stop service with kill file.");

        try {
            FileUtils.copyFileToDirectory(sourceFile, destinationDirectory);
            logger.log(Level.WARNING, "Copied kill file {0} to {1}", new Object[]{sourceFile, destinationDirectoryPath});
            
            State state = State.INDETERMINATE;
            
            final long startTime = System.currentTimeMillis();
            
            while ((System.currentTimeMillis() - startTime) < timeout) {
                state = queryService(virtualMachineName);
                
                if (state == State.STOPPED) {
                    logger.log(Level.WARNING, "Kill service: Virtual machine {0} stopped.", virtualMachineName);
                    break;
                } else if (state == State.ACCESS_DENIED) {
                    logger.log(Level.WARNING, "Kill service: Virtual machine {0} access denied.", virtualMachineName);
                    break;
                }
            }
            
            return state;
        } catch (IOException ex) {
            logger.log(Level.WARNING, "message: {0}", ex.getLocalizedMessage());
            logger.log(Level.WARNING, "Cannot kill virtual machine: {0}: {1}", 
                    new Object[]{virtualMachineName, ex.getLocalizedMessage()});
            return State.INDETERMINATE;
        }
    }

    /**
     * Checks whether service is running or not
     * 
     * @param virtualMachineName  remote virtual machine name
     * @return 
     */
    private State queryService(String virtualMachineName) {
        final Runtime runtime = Runtime.getRuntime();
                
        try {
            final String queryCommand = "sc \\\\" + virtualMachineName + " query " + serviceName;
            logger.log(Level.WARNING, "Running command: {0}", queryCommand);
            
            final Process process = runtime.exec(queryCommand);
            
            final InputStream inputStream = process.getInputStream();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            
            while((line = reader.readLine()) != null) {
                if (line.matches(".*RUNNING.*")) {
                    logger.log(Level.WARNING, "Query service: Virtual machine {0} running.", virtualMachineName);
                    return State.RUNNING; 
                } else if (line.matches(".*STOPPED.*")) {
                    logger.log(Level.WARNING, "Query service: Virtual machine {0} stopped.", virtualMachineName);
                    return State.STOPPED; 
                } else if (line.matches(".*Access is denied.*")) {
                    logger.log(Level.WARNING, "Query service: Virtual machine {0} access denied.", virtualMachineName);
                    return State.ACCESS_DENIED; 
                }
                
                // sburns - testing only
                /*
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Service.class.getName()).logger(Level.SEVERE, null, ex);
                }
                */
            }
                
            return State.INDETERMINATE;
        } catch (IOException ex) {
            logger.log(Level.WARNING, "message: {0}", ex.getLocalizedMessage());
            logger.log(Level.WARNING, "Cannot query virtual machine: {0}: {1}", new Object[]{virtualMachineName, ex.getLocalizedMessage()});
            return State.INDETERMINATE;
        }   
     }
}
    


