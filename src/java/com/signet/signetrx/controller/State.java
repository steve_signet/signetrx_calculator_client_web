/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.controller;

/**
 *
 * @author sburns
 */
public enum State {
    RUNNING, STOPPED, ACCESS_DENIED, INDETERMINATE
}
