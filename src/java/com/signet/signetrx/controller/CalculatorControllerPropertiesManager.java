/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author sburns
 */
public class CalculatorControllerPropertiesManager {
    private static final Logger logger = Logger.getLogger("calculatorcontroller.log");
    private final Properties properties;
    private final static String CALCULATOR_CONTROLLER_PROPERTIES = "src/com/signet/signetrx/controller/resources/calculator_controller.properties";

    public CalculatorControllerPropertiesManager() {
        properties = new Properties();
    }
    
    public Properties loadParams() {
        final InputStream fileInputStream;

        try {
            final File file = new File(CALCULATOR_CONTROLLER_PROPERTIES);
            fileInputStream = new FileInputStream(file);

            properties.load(fileInputStream);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to load directories params: {0}", ex.getLocalizedMessage());
            
            JOptionPane.showMessageDialog(null, "Unable to load directories params.",
                "Properties Error", JOptionPane.ERROR_MESSAGE);            
        }
        
        return properties;
    }
}
