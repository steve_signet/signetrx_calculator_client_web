/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.signet.signetrx.controller;

import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sburns
 */
@Named(value = "virtualMachineList")
@SessionScoped
public class VirtualMachineList extends ArrayList<VirtualMachine> implements Serializable {
    private static final Logger logger = Logger.getLogger("calculatorcontroller.log");
    
    /**
     * Creates a new instance of VirtualMachineList
     */
    public VirtualMachineList() { 
        VirtualMachinePropertiesManager virtualMachinePropertiesManager = new VirtualMachinePropertiesManager();

        final VirtualMachineProperties properties;
        
        try {
            properties = virtualMachinePropertiesManager.loadParams();
        
            Set<String> keySet = properties.keySet();

            for (String key: keySet) {
                String value = (String) properties.get(key);
                add(new VirtualMachine(value));
            }

            virtualMachinePropertiesManager.storeParams(properties);
        } catch (IOException ex) {
            Logger.getLogger(VirtualMachineList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    // Button commands
    
    public String start() {       
        for (VirtualMachine virtualMachine: this) {
            if (virtualMachine.getChecked().equals("true")) {
                virtualMachine.start();
                virtualMachine.setStatus("Running");
            }
        }
        
        return "index";
    }
    
    public String stop() {
        for (VirtualMachine virtualMachine: this) {
            if (virtualMachine.getChecked().equals("true")) {
                virtualMachine.stop();
                virtualMachine.setStatus("Stopped");
            }
        }

        return "index";
    }
    
    public String query() {
        for (VirtualMachine virtualMachine: this) {
            if (virtualMachine.getChecked().equals("true")) {
                virtualMachine.query();
                virtualMachine.setStatus("Running");
            }
        }

        return "index";
    }
    
    public String add() {
        return "index";
    }
    
    public String remove() {
        return "index";
    }
    
    public String browse() {
        return "indexl";
    }
    
    public String update() {
        return "index";
    }
}
