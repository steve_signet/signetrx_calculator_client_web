/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.signet.signetrx.controller;

import java.io.Serializable;

/**
 *
 * @author sburns
 */
public class VirtualMachine implements Serializable {
    private String checked = "false";
    private String name = "SIGRX-CE1";
    private String status = "Indeterminate";
    private String jarFileVersion = "1.0.0";
    private String progress = "50%";
    
    /**
     * Creates a new instance of VirtualMachineManagedBean
     */
    public VirtualMachine() {
    }

    public VirtualMachine(String name) {
        super();
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the checked
     */
    public String getChecked() {
        return checked;
    }

    /**
     * @param checked the checked to set
     */
    public void setChecked(String checked) {
        this.checked = checked;
    }

    /**
     * @return the jarFileVersion
     */
    public String getJarFileVersion() {
        return jarFileVersion;
    }

    /**
     * @param jarFileVersion the jarFileVersion to set
     */
    public void setJarFileVersion(String jarFileVersion) {
        this.jarFileVersion = jarFileVersion;
    }

    /**
     * @return the progress
     */
    public String getProgress() {
        return progress;
    }

    /**
     * @param progress the progress to set
     */
    public void setProgress(String progress) {
        this.progress = progress;
    }
    
    // Button commands
    
    public boolean start() {
        String command = "start";
        new Thread(new Service(command, this)).start();
        status = "Running";
        
        return true;
    }
    
    public boolean stop() {
        String command = "stop";
        new Thread(new Service(command, this)).start();
        status = "Stopped";
        
        return true;
    }
    
    public boolean query() {
        
        String command = "query";
        new Thread(new Service(command, this)).start();
        status = "Access Denied";
        
        return true;
    }
    
    public boolean add() {
        //status = "Indeterminate";
        return true;
    }    

    public boolean remove() {
        /*
        String command = "remove";
        new Thread(new Service(command, this)).start();
        status = "Indeterminate"
         */
        return true;
    }
    
    public boolean browse() {
        /*
        String command = "remove";
        new Thread(new Service(command, this)).start();
        status = "Indeterminate"
         */
        return true;
    }
    
    public boolean update() {
        /*
        String command = "remove";
        new Thread(new Service(command, this)).start();
        status = "Indeterminate"
         */
        return true;
    }
}
