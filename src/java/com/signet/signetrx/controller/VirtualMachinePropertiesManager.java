/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/**
 *
 * @author sburns
 */
public class VirtualMachinePropertiesManager {
    private static final Logger logger = Logger.getLogger("calculatorcontroller.log");
    private final VirtualMachineProperties properties;
    private final static String VIRTUAL_MACHINE_PROPERTIES = "/WEB-INF/virtual_machine.properties";

    public VirtualMachinePropertiesManager() {
        properties = new VirtualMachineProperties();
    }
    
    public VirtualMachineProperties loadParams() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        properties.load(ec.getResourceAsStream(VIRTUAL_MACHINE_PROPERTIES));        
                
        return properties;
    }    
    
    public VirtualMachineProperties storeParams(VirtualMachineProperties properties) throws IOException {                
        ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String path = context.getRealPath(VIRTUAL_MACHINE_PROPERTIES);   

        OutputStream fileOutputStream;
                
        try {
            final File file = new File(path);
            fileOutputStream = new FileOutputStream(file);
                        
            properties.store(fileOutputStream, "Virtual Machine Properties List");
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to store params: {0}", ex.getLocalizedMessage());
        }
        
        return properties;
    }
}
