/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.controller;

import java.util.Comparator;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author steve
 */
public class VirtualMachineProperties extends Properties {    
    @Override
    public Set keySet() {
        final CustomStringComparator customStringComparator = new CustomStringComparator();
        final SortedSet sortedSet = new TreeSet(new CustomStringComparator());

        final Set keySet = super.keySet();
        
        for (Object value: keySet) {
            sortedSet.add(value);
        }
        
        return sortedSet;
    }
    
    class CustomStringComparator implements Comparator<String>{
        @Override
        public int compare(String str1, String str2) {

           // extract numeric portion out of the string and convert them to int
           // and compare them

           final int num1 = Integer.parseInt(str1.substring(5));
           final int num2 = Integer.parseInt(str2.substring(5));

           return num1 - num2;

        }
    }    
}
