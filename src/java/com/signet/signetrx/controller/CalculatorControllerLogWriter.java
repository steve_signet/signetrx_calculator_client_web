/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.controller;

import com.signet.commons.logging.LogWriter;
import java.net.URL;
import org.apache.log4j.LogManager;

/**
 *
 * @author sburns
 */

public class CalculatorControllerLogWriter extends LogWriter {
    
    private static final URL configUrl = 
            ClassLoader.getSystemResource("com/signet/signetrx/controller/resources/logging.properties");
    
    private static final CalculatorControllerLogWriter instance = 
            new CalculatorControllerLogWriter();
    
    public CalculatorControllerLogWriter() {
        super(configUrl);
        super.addLogger(LogManager.getLogger("AppLogFile"));
        super.addLogger(LogManager.getLogger("SystemOut"));
        super.addLogger(LogManager.getLogger("TextPane"));
    }
    
    public static CalculatorControllerLogWriter getInstance() {
        return instance;
    }
}

